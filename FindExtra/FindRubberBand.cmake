find_path(RubberBand_INCLUDE_DIR NAMES rubberband/RubberBandStretcher.h REQUIRED)
find_library(RubberBand_LIBRARY NAMES rubberband REQUIRED)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RubberBand DEFAULT_MSG RubberBand_LIBRARY RubberBand_INCLUDE_DIR)

if (RubberBand_FOUND)
    set(RubberBand_INCLUDE_DIRS ${RubberBand_INCLUDE_DIR})
    set(RubberBand_LIBRARIES ${RubberBand_LIBRARY})
else ()
    set(RubberBand_LIBRARIES)
endif ()

mark_as_advanced(RubberBand_INCLUDE_DIR RubberBand_LIBRARY)
