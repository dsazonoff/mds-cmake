include_guard(DIRECTORY)

include(${CMAKE_SOURCE_DIR}/mds-cmake/mds-variables.cmake)
include(${CMAKE_SOURCE_DIR}/mds-cmake/mds-utils.cmake)


macro(mds_find_qt)
    list(APPEND qt_components Core Concurrent Gui Multimedia Widgets Quick QuickControls2 Svg)
    set(qt_libraries)
    foreach (component ${qt_components})
        list(APPEND qt_libraries "Qt6::${component}")
    endforeach ()
    # set(qt_compile_definitions $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
    set(find_qt_extra)
    if (IS_DIRECTORY "$ENV{QT_DIR}")
        set(find_qt_extra NO_DEFAULT_PATH PATHS "$ENV{QT_DIR}")
    endif ()

    find_package(Qt6 COMPONENTS ${qt_components} REQUIRED ${find_qt_extra})

    qt_policy(SET QTP0001 NEW)
    qt_standard_project_setup(REQUIRES 6.6)
endmacro()


function(mds_define_qt_target target)
    set(args_option
        APPLICATION
        LIBRARY
        QML
    )
    set(args_single
        VERSION
        BIN_NAME
    )
    set(args_multi
    )

    cmake_parse_arguments(PARSE_ARGV 1 arg "${args_option}" "${args_single}" "${args_multi}")
    if (arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown/unexpected arguments: ${arg_UNPARSED_ARGUMENTS}")
    endif ()

    enumerate_files(source_files
        PATH "${CMAKE_CURRENT_SOURCE_DIR}"
        BASE_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
        EXTENSIONS ${mds_default_sources_ext}
    )

    if (arg_APPLICATION AND arg_LIBRARY)
        message(FATAL_ERROR "One of APPLICATION or LIBRARY should be specified")
    endif ()
    if (arg_APPLICATION)
        qt_add_executable(${target_name})
        target_sources(${target} PRIVATE ${source_files})
    elseif (arg_LIBRARY)
        #qt_add_library(${target_name} STATIC)
        set(use_static STATIC)
    else ()
        message(FATAL_ERROR "One of APPLICATION or LIBRARY should be specified")
    endif ()

    if (arg_QML)
        enumerate_files(qml_files
            PATH "${CMAKE_CURRENT_SOURCE_DIR}"
            BASE_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
            EXTENSIONS qml
        )
        cmake_path(RELATIVE_PATH CMAKE_CURRENT_BINARY_DIR BASE_DIRECTORY "${CMAKE_BINARY_DIR}" OUTPUT_VARIABLE module_path)

        if (NOT DEFINED arg_VERSION)
            set(arg_VERSION ${PROJECT_VERSION})
        endif ()
        qt_add_qml_module(${target}
            URI ${target}
            VERSION ${arg_VERSION}
            ${use_static}
            QML_FILES ${qml_files}
            SOURCES ${source_files}
            DEPENDENCIES ${qt_components}
            # TODO: investigate
            # TARGET_PATH "${module_path}" # Internal argument, prevents unwanted warnings
            # OUTPUT_DIRECTORY "${module_path}"
        )

        if (arg_LIBRARY)
             mds_target_include_directories(TARGET ${target}plugin)
             mds_target_default_pch(TARGET ${target}plugin)
        endif ()

        qt_import_qml_plugins(${target})
    endif ()

    # Configuration
    mds_target_compile_configuration(TARGET ${target})
    mds_target_include_directories(TARGET ${target})
    mds_target_default_pch(TARGET ${target})
    target_compile_definitions(${target} PRIVATE ${qt_compile_definitions})
    target_link_libraries(${target} PRIVATE ${qt_libraries})

    if (DEFINED arg_BIN_NAME)
        set_target_properties(${target} PROPERTIES OUTPUT_NAME "${arg_BIN_NAME}")
    endif ()

endfunction()


function(mds_target_deploy target)
    set(args_option
    )
    set(args_single
        ICON
        NAME
        ID
        VERSION
        COPYRIGHT
    )
    set(args_multi
    )

    cmake_parse_arguments(PARSE_ARGV 1 arg "${args_option}" "${args_single}" "${args_multi}")
    if (arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown/unexpected arguments: ${arg_UNPARSED_ARGUMENTS}")
    endif ()

    if (DEFINED arg_ICON)
        set_source_files_properties(${arg_ICON} PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
        target_sources(${target} PRIVATE ${arg_ICON})
    endif ()

    if (
    (NOT DEFINED arg_NAME) OR
    (NOT DEFINED arg_ID) OR
    (NOT DEFINED arg_VERSION) OR
    (NOT DEFINED arg_COPYRIGHT)
    )
        message(FATA_ERROR "Following arguments should be set: ${args_single}")
    endif ()
    set_target_properties(${target} PROPERTIES
        MACOSX_BUNDLE TRUE
        MACOSX_BUNDLE_BUNDLE_NAME "${arg_NAME}"
        MACOSX_BUNDLE_GUI_IDENTIFIER "${arg_ID}"
        MACOSX_BUNDLE_BUNDLE_VERSION "${arg_VERSION}"
        MACOSX_BUNDLE_COPYRIGHT "${arg_COPYRIGHT}"
    )

    install(TARGETS ${target_name}
        BUNDLE DESTINATION .
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    )
    qt_generate_deploy_qml_app_script(
        TARGET ${target_name}
        OUTPUT_SCRIPT deploy_script
    )
    install(SCRIPT ${deploy_script})
endfunction()
