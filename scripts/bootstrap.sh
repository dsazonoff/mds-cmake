#!/bin/bash

set -e

# Variables
command="$0"
manifest_root=.
overlay_ports=${manifest_root}/vcpkg_registry/ports
triplet=""

# Command line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
  --manifest-root)
    if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
      manifest_root="$2"
      shift
    else
      echo "$1: missing argument" >&2
      exit 1
    fi
    ;;
  --triplet)
    if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
      triplet="--triplet $2"
      shift
    else
      echo "$1: missing argument" >&2
      exit 1
    fi
    ;;
  -*=)
    printf "Unknown parameter passed: %s
Usage: %s
[--manifest-root]         Set manifest root (location of vcpkg.json).
[--triplet]               Custom triplet for vcpkg https://github.com/microsoft/vcpkg/tree/master/triplets
" "$1" "$command"
    exit 1
    ;;
  esac
  shift
done

if [ ! -e "${manifest_root}/vcpkg.json" ]; then
  echo "${manifest_root}/vcpkg.json file not found"
  exit 1
fi

if [ -d "$DIRECTORY" ]; then
  overlay_ports=--overlay-ports="${overlay_ports_root}"
  else
  overlay_ports=""
fi

# mkdir -p "${overlay_ports_root}"

# Clone and build vcpkg, if necessary
vcpkg_repo=https://github.com/microsoft/vcpkg.git
if [ -z "${VCPKG_ROOT}" ]; then
  VCPKG_ROOT=$(realpath "../vcpkg")
fi
if [ ! -f "${VCPKG_ROOT}"/bootstrap-vcpkg.sh ]; then
  mkdir -p "${VCPKG_ROOT}"
  pushd "${VCPKG_ROOT}/.."
  rm -rf vcpkg
  git clone ${vcpkg_repo}
  popd
else
  pushd "${VCPKG_ROOT}"
  git fetch
  commits_count=$(git rev-list HEAD...origin/master --count)
  if [ "${commits_count}" -ne 0 ]; then
    git pull
    rm -rf "${VCPKG_ROOT}"/vcpkg
  fi
  popd
fi
if [ ! -f "${VCPKG_ROOT}"/vcpkg ]; then
  "${VCPKG_ROOT}"/bootstrap-vcpkg.sh -disableMetrics
fi

echo "${VCPKG_ROOT}/vcpkg" install "${triplet}" "${overlay_ports}" --feature-flags=manifests --x-install-root="${VCPKG_ROOT}/installed" --x-manifest-root="${manifest_root}"
