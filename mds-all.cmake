include_guard(DIRECTORY)

list(APPEND CMAKE_MODULE_PATH
    "${CMAKE_SOURCE_DIR}/mds-cmake"
    "${CMAKE_SOURCE_DIR}/mds-cmake/FindExtra"
)

# Global project configuration
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_INCLUDE_CURRENT_DIR OFF)
set(CMAKE_COLOR_DIAGNOSTICS ON)

# Default install into the build directory
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/distr/${CMAKE_HOST_SYSTEM_NAME}-${CMAKE_BUILD_TYPE}" CACHE PATH "" FORCE)
endif ()


include(${CMAKE_CURRENT_LIST_DIR}/mds-variables.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/mds-utils.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/thirdparty/qt.cmake)
